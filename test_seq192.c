/** @file test_seq192.c
 *  @brief A naive interface to perform AES-192 encryption
 *  
 *  A naive AES-192 interface to utilize "aes_core.c", an OpenSSL based implementation of 
 *  AES cryptographic library. Provides a 192-bit key (24 bytes) to key expansion functions. 
 *  Reads "plaintext.txt" block-wise (16 bytes) for encryption using encrypt function of 
 *  "aes_core.c". Encrypted output is written block-wise to the "plaintext_encrypted.bin",
 *  a binary file. In decryption, "plaintext_encrypted.bin" is similarly decrypted block-wise 
 *  using decrypt function, and output written to file "plaintext_decrypted.txt". 
 *  
 *  @author Raghu Mulukutla <raghu94@cmu.edu>
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "aes.h"


int main()
{

    //================================================================================
    // Common Declarations
    //================================================================================

    static const unsigned char key[24] = "hello, AES block cipher!";

    unsigned char buff[16];
    unsigned char enc_out[16];
    unsigned char dec_out[16];
    clock_t begin, end;
    double time_elapsed;


    //================================================================================
    // Encryption
    //================================================================================

    FILE *fpr;
    FILE *fpwe;

    fpr = fopen("plaintext.txt", "r");
    fpwe = fopen("plaintext_encrypted.bin", "wb");


    AES_KEY enc_key, dec_key;

    printf("\nEncrypting with 192-bit key :\t");
    for (int i = 0; i < 24; ++i)
        printf("%c",key[i]);
    printf("\n\n");
    
    begin = clock();

    AES_set_encrypt_key(key, 192, &enc_key);

    while(fgets(buff, 16, fpr)!=NULL) {
        AES_encrypt(buff, enc_out, &enc_key); 
        fwrite(enc_out, sizeof(enc_out), 1, fpwe);
    }

    end = clock();
    time_elapsed = (double)(end - begin) / CLOCKS_PER_SEC;
    
    fclose(fpr);
    fclose(fpwe);

    printf("Encryption has completed in %f seconds.\n\n", time_elapsed);



    //================================================================================
    // Decryption
    //================================================================================
    
    FILE *fpre;
    FILE *fwd;

    fpre = fopen("plaintext_encrypted.bin", "rb");
    fwd = fopen("plaintext_decrypted.txt", "w");

    printf("Decrypting with 192-bit key :\t");
    for (int i = 0; i < 24; ++i)
        printf("%c",key[i]);
    printf("\n\n");

    begin = clock();

    AES_set_decrypt_key(key,192, &dec_key);

    while(fread(buff, sizeof(buff), 1, fpre)) {
        AES_decrypt(buff, dec_out, &dec_key);  
        fprintf(fwd,"%s",dec_out);
    }

    end = clock();
    time_elapsed = (double)(end - begin) / CLOCKS_PER_SEC;

    fclose(fpre);
    fclose(fwd);

    printf("Decryption has completed in %f seconds.\n\n", time_elapsed);


    return 0;
}
