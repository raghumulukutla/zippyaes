#ifndef AES_NI_H_INCLUDED
# define AES_NI_H_INCLUDED

#if !defined (ALIGN16)
# if defined (__GNUC__)
# define ALIGN16 __attribute__ ( (aligned (16)))
# else
# define ALIGN16 __declspec (align (16))
# endif
#endif


typedef struct KEY_SCHEDULE{
    ALIGN16 unsigned char KEY[16*15];
    unsigned int nr;
} AES_KEY;

//Key assist functions from Intel
__m128i AES_128_ASSIST(__m128i temp1, __m128i temp2);
void KEY_192_ASSIST(__m128i* temp1, __m128i * temp2, __m128i * temp3);
void KEY_256_ASSIST_1(__m128i* temp1, __m128i * temp2);
void KEY_256_ASSIST_2(__m128i* temp1, __m128i * temp3);

//Key expander functions from Intel
void AES_128_Key_Expansion(const unsigned char *userkey, unsigned char *key);
void AES_192_Key_Expansion (const unsigned char *userkey, unsigned char *key);
void AES_256_Key_Expansion (const unsigned char *userkey, unsigned char *key);

//Encryption
int AES_set_encrypt_key(const unsigned char *userKey, const int bits, AES_KEY *key);
void AES_encrypt(const unsigned char *in, unsigned char *out, unsigned long length, const char *key, int number_of_rounds);

//Decryption
int AES_set_decrypt_key (const unsigned char *userKey, const int bits, AES_KEY *key);
void AES_decrypt(const unsigned char *in, unsigned char *out, unsigned long length, const char *key, int number_of_rounds);
#endif