import java.io.*;
import java.nio.file.*;
import java.nio.charset.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;



public class PlainTextGenerator {
    private List<String> wordList = new ArrayList<String>();


    public static void main(String args[]) {

        if(args.length < 2){
            System.out.println("Invalid file size parametersss");
            System.exit(0);
        }

        int fileLength = Integer.parseInt(args[0]);
        String sizeUnit = args[1];
        sizeUnit = sizeUnit.toLowerCase();
        System.out.println(sizeUnit);

        if (sizeUnit.equals("k")) {
            fileLength = fileLength * 1024;
        } else if (sizeUnit.equals("m")) {
            fileLength = fileLength * 1024 * 1024;
        } else if (sizeUnit.equals("g")) {
            fileLength = fileLength * 1024 * 1024 * 1024;
        }
        else {
            System.out.println("Invalid file size parameters");
            System.exit(0);
        }
        PlainTextGenerator plainTextGenerator = new PlainTextGenerator();
        plainTextGenerator.readFile();
        Path filePath = Paths.get("plaintext.txt");
        File file = new File("wordlist.txt");
        List<String> lineList = new ArrayList<String>();
        int wordCount = 0;
        String line = "";
        while(wordCount <= fileLength ) {
            Random randGen = new Random();
            String word = plainTextGenerator.wordList.get(randGen.nextInt(1306));
            line = line + word + " " ;
            wordCount += word.length() + 1;
            
        }
        lineList.add(line);

        try {
            Files.write(filePath, lineList);
        }
        catch(IOException ex) {

        }
    }

    public void readFile()
    {
        // The name of the file to open.
        try {
            ClassLoader.getSystemClassLoader().getResource("wordlist.txt");


        }
        catch(IOError ex){
            System.out.println("Resource cannot be loaded");
        }
        URL fileName =  ClassLoader.getSystemClassLoader().getResource("wordlist.txt");
        ;
        
        int wordListID = 0;
        // This will reference one line at a time
        String line = null;


        try {

            // Always wrap FileReader in BufferedReader.
            BufferedReader bufferedReader =
                new BufferedReader(new InputStreamReader(fileName.openStream()));

            while((line = bufferedReader.readLine()) != null) {
                wordList.add(line);
                wordListID++;
                //System.out.println(line);

            }
            // Always close files.
            bufferedReader.close();
        }
        catch(FileNotFoundException ex) {
            System.out.println(
                "Unable to open file '" + 
                fileName + "'");                
        }
        catch(IOException ex) {
            System.out.println(
                "Error reading file '" 
                + fileName + "'");                  
            // Or we could just do this: 
            // ex.printStackTrace();
        }
    }
 }
    