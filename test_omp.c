#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <omp.h>
#include <wmmintrin.h>
#include "aes.h"


int main()
{

    static const unsigned char key[16] = "hey cryptography";

    unsigned char *rbuff; //read buffer for slurping input file
    unsigned char *wbuff; // write buffer for writing encrypted file
    unsigned char enc_in[16];
    unsigned char enc_out[16];
    unsigned char dec_out[16];
    unsigned char dec_in[16];
    clock_t begin, end;
    double time_elapsed;
    int length;

    FILE *fpr;
    FILE *fpwe;
    


    fpr = fopen("plaintext.txt", "r");  //input
    fpwe = fopen("plaintext_encrypted.bin", "wb"); //encrypted output


    if(fpr == NULL) {
        
        return(-1);
    }

    fseek(fpr, 0, SEEK_END);

    length = ftell(fpr);

    printf("Total file size is = %d bytes\n", length );

    fseek(fpr, 0, SEEK_SET);


    rbuff = malloc(sizeof(unsigned char)*length);
    wbuff = malloc(sizeof(unsigned char)*length);


//---------------------------------ENCRYPTION------------------------------------//

    AES_KEY enc_key, dec_key;

    printf("Encrypting...\n");
    begin = clock();
    AES_set_encrypt_key(key, 128, &enc_key);

    if(rbuff) 
        fread(rbuff, sizeof(unsigned char), length, fpr); // read input file into rbuff
    

    

   // length = strlen(rbuff);
    //printf("%s\n",rbuff );


   #pragma omp parallel for schedule(static, 100000) num_threads(4) private(enc_out, enc_in)
    for(int i=0; i < length; i=i+16) {
        //printf("%d\n",omp_get_thread_num() );
        memcpy(enc_in, (rbuff+i), 16);
        AES_encrypt(enc_in, enc_out, &enc_key);
        memcpy((wbuff+i), enc_out, sizeof(enc_out));
        //fwrite(enc_out, sizeof(unsigned char), sizeof(enc_out), fpwe);
    }



  
    fwrite(wbuff, sizeof(unsigned char), length, fpwe);

   


    end = clock();
    time_elapsed = (double)(end - begin) / CLOCKS_PER_SEC;
    free(wbuff);
    free(rbuff);
    fclose(fpr);
    fclose(fpwe);



    printf("Encryption completed in %f seconds\n", time_elapsed);

    //return 0;
    
// -------------------------------DECRYPTION------------------------------------//

    FILE *fpre;
    FILE *fwd;

    

    fpre = fopen("plaintext_encrypted.bin", "rb");
    fwd = fopen("plaintext_decrypted.txt", "a");

    printf("Decrypting....\n");
    fseek(fpre,0, SEEK_END);
    length = ftell(fpre);
    printf("%d\n",length );
    fseek(fpre,0, SEEK_SET);
    rbuff = malloc(sizeof(unsigned char)*length);
    wbuff = malloc(sizeof(unsigned char)*length);
    
    if(rbuff) 
        fread(rbuff, sizeof(unsigned char), length, fpre); // read input file into rbuff
    
    
    begin = clock();

    AES_set_decrypt_key(key,128, &dec_key);
    for (int i = 0; i < length; i=i+16) 
    {
        memcpy(dec_in, (rbuff+i),16);
        AES_decrypt(dec_in, dec_out, &dec_key);
        memcpy((wbuff+i), dec_out, sizeof(dec_out));
    }
    
    fwrite(wbuff, sizeof(unsigned char), length, fwd );

    fclose(fpre);
    fclose(fwd);
    free(rbuff);
    free(wbuff);

    end = clock();
    time_elapsed = (double)(end - begin) / CLOCKS_PER_SEC;
    printf("Decryption completed in %f seconds\n", time_elapsed);


    return 0;
}
