/** @file test_ni128.c
 *  @brief An interface to perform AES-128 encryption using the Intel® AES-NI Instruction Set
 *  
 *  A hardware accerlerated AES-128 interface utilizing "aes_ni.c", a C implementation of 
 *  Intel® AES-NI Instruction Set. Provides a 128-bit key (16 bytes) to key expansion functions. 
 *  Slurps "plaintext.txt" into read buffer and uses special AES-NI encrypt function for
 *  encryption. Encrypted output is written to the binary file "plaintext_encrypted.bin",
 *  In decryption, "plaintext_encrypted.bin" is similarly slurped, decrypted using special 
 *  AES-NI decrypt function, and buffer written to file "plaintext_decrypted.txt". 
 *  
 *  @author Raghu Mulukutla <raghu94@cmu.edu>
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <wmmintrin.h>
#include "aes_ni.h"



int main()
{

    //================================================================================
    // Common Declarations
    //================================================================================

    static const unsigned char key[16] = "hey cryptography";

    unsigned char *rbuff; //read buffer for slurping input file
    unsigned char *wbuff; // write buffer for writing encrypted file
    clock_t begin, end;
    double time_elapsed;
    int length;
    int i;



    //================================================================================
    // Encryption
    //================================================================================

    FILE *fpr;
    FILE *fpwe;
    


    fpr = fopen("plaintext.txt", "r");  //input
    fpwe = fopen("plaintext_encrypted.bin", "wb"); //encrypted output


    if(fpr == NULL)
        return(-1);

    fseek(fpr, 0, SEEK_END);

    length = ftell(fpr);

    fseek(fpr, 0, SEEK_SET);


    rbuff = malloc(sizeof(unsigned char)*length);
    wbuff = malloc(sizeof(unsigned char)*length);
    

    AES_KEY enc_key, dec_key;

    printf("\nEncrypting with 128-bit key :\t");
    for (i = 0; i < 16; ++i)
        printf("%c",key[i]);
    printf("\n\n");

    begin = clock();
    AES_set_encrypt_key(key, 128, &enc_key);

    if(rbuff) 
        fread(rbuff, sizeof(unsigned char), length, fpr); // read input file into rbuff
    

    AES_encrypt(rbuff, wbuff, length, enc_key.KEY, enc_key.nr);
    fwrite(wbuff, sizeof(unsigned char), length, fpwe);

   

    end = clock();
    time_elapsed = (double)(end - begin) / CLOCKS_PER_SEC;
    free(wbuff);
    free(rbuff);
    fclose(fpr);
    fclose(fpwe);



    printf("Encryption has completed in %f seconds\n", time_elapsed);

    
    //================================================================================
    // Decryption
    //================================================================================

    FILE *fpre;
    FILE *fwd;

    

    fpre = fopen("plaintext_encrypted.bin", "rb");
    fwd = fopen("plaintext_decrypted.txt", "w");

    printf("\nEncrypting with 128-bit key :\t");
    for (i = 0; i < 16; ++i)
        printf("%c",key[i]);
    printf("\n\n");

    fseek(fpre,0, SEEK_END);
    length = ftell(fpre);
    fseek(fpre,0, SEEK_SET);
    rbuff = malloc(sizeof(unsigned char)*length);
    wbuff = malloc(sizeof(unsigned char)*length);
    
    
    begin = clock();
    AES_set_decrypt_key(key,128, &dec_key);

    if(rbuff) 
        fread(rbuff, sizeof(unsigned char), length, fpre); // read input file into rbuff

    
    AES_decrypt(rbuff, wbuff, length, dec_key.KEY, dec_key.nr);
    fwrite(wbuff, sizeof(unsigned char), length, fwd );

    end = clock();
    time_elapsed = (double)(end - begin) / CLOCKS_PER_SEC;

    free(rbuff);
    free(wbuff);
    fclose(fpre);
    fclose(fwd);

   
    printf("Decryption has completed in %f seconds\n\n", time_elapsed);


    return 0;
}
